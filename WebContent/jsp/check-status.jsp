<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>User</title>
 <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/Assignment4/user">Package Delivery</a>
    </div>
      <ul class="nav navbar-nav">
      <li  ><a href="/Assignment4/user">My Packages</a></li>
      <li  ><a href="/Assignment4/user">Search packages</a></li>
      <li  ><a href="/Assignment4/user">Check status for package</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/Assignment4/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>


			<label>Route entries for the package:</label><br>
  			<c:forEach var="entryItem" items="${entries}">  
					<label>Date:</label><ins>${entryItem.date }</ins><br>
					<label>City:</label><ins>${entryItem.city }</ins><br>
				<hr>
 		</c:forEach>  
 		<a href="/Assignment4/user"><input class="btn btn-default" type="button"
					value="Back" /></a>
					<ins>${msg}</ins>
  		
</body>
</html>