<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Administrator Page</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Package Delivery</a>
    </div>
    <ul class="nav navbar-nav">
      <li  ><a href="/Assignment4/admin/package">Add Package</a></li>
      <li  ><a href="/Assignment4/admin/create-route">Update Route</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/Assignment4/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	<!-- End mainmenu area -->

	<div class="product-big-title-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="product-bit-title text-center">
						<h2>Administrator</h2>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="packages">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Package Sender</th>
					<th>Package Receiver</th>
					<th>Name</th>
					<th>Sender City</th>
					<th>Destination City</th>
					<th>Description</th>
					<th>Tracking</th>
					<th>Delete</th>
						<th>Register for Tracking</th>
				</tr>
			</thead>
			<c:forEach var="packageItem" items="${packages}">
				<tr>
					<td><ins>${packageItem.id }</ins></td>
					<td><ins>${packageItem.sender }</ins></td>
					<td><ins>${packageItem.receiver }</ins></td>
					<td><ins>${packageItem.name}</ins></td>
					<td><ins>${packageItem.senderCity}</ins></td>
					<td><ins>${packageItem.destinationCity}</ins></td>
					<td><ins>${packageItem.description}</ins></td>
					<td><ins>${packageItem.tracking}</ins></td>
					<td><ins>
							<a href="/Assignment4/admin/delete/${packageItem.id}">Delete</a>
						</ins></td>
						<td><ins>
							<a href="/Assignment4/admin/register-for-tracking/${packageItem.id}">Track</a>
						</ins></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>


	
</body>
</html>