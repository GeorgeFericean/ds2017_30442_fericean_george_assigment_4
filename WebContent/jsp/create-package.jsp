<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Flight</title>
 <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/Assignment4/admin">Package Tracking</a>
    </div>
    <ul class="nav navbar-nav">
       <li ><a href="/Assignment4/admin/package">Add Package</a></li>
         <li  ><a href="/Assignment4/admin/create-route">Update Route</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/Assignment4/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	<div id="login">
	<div align="center">
			<font size="3" color="red"><br>${msg}</font>
		</div>
		<form class="form-horizontal" name='package' action="<c:url value='/admin/package' />"
			method='POST'>

			 <div class="form-group">
			 <label class="control-label col-sm-2">Sender</label>
				<input type="text" id="user" name="sender"
				class="col-sm-6"
					placeholder="Sender">
			</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2">Receiver</label>
				<input type="text" id="user" name="receiver"
				class="col-sm-6"
					placeholder="Receiver">
			</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2">Name</label>
				<input type="text" id="user" name="name"
				class="col-sm-6"
					placeholder="Name">
			</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2">Sender City</label>
				<input type="text" id="user" name="senderCity"
				class="col-sm-6"
					placeholder="Sender City">
			</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2">Destination City</label>
				<input type="text" id="user" name="destinationCity"
				class="col-sm-6"
					placeholder="Destination City">
			</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2">Description</label>
				<input type="text" id="user" name="description"
				class="col-sm-6"
					placeholder="Description">
			</div>
			
			<div class="button-container">
			<input class="btn btn-default" name="Add" type="submit" value="Add" />
			<a href="/Assignment4/admin"><input class="btn btn-default" type="button"
					value="Back" /></a>
			</div>

		</form>
		
	</div>
</body>
</html>