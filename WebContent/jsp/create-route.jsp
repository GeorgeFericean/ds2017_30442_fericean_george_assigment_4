<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Flight</title>
 <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Package Delivery</a>
    </div>
    <ul class="nav navbar-nav">
      <li  ><a href="/Assignment4/admin/package">Add Package</a></li>
      <li  ><a href="/Assignment4/admin/create-route">Update Route</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/Assignment4/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	<div id="login">
	<div align="center">
			<font size="3" color="red"><br>${msg}</font>
		</div>
		<form class="form-horizontal" name='route' action="<c:url value='/admin/create-route' />"
			method='POST'>

			 <div class="form-group">
			 <label class="control-label col-sm-2">Package ID</label>
				<input type="text" id="user" name="id"
				class="col-sm-6"
					placeholder="Package ID">
					</div>
			 <div class="form-group">
			 <label class="control-label col-sm-2"> City</label>
				<input type="text" id="user" name="city"
				class="col-sm-6"
					placeholder="City">
			</div>
			
			<div class="button-container">
			<input class="btn btn-default" name="Add" type="submit" value="Add" />
			<a href="/Assignment4/admin"><input class="btn btn-default" type="button"
					value="Back" /></a>
			</div>

		</form>
		
	</div>
</body>
</html>