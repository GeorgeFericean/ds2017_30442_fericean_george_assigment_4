package webServices;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import entities.Package;
import entities.User;
import exception.EntityDoesNotExistException;
import exception.FieldsMandatoryException;
@WebService
@SOAPBinding(style = Style.DOCUMENT)  
public interface AdminWebService {
	@WebMethod List<User> findAllUsers();
    @WebMethod User findByUserId(int id);
    @WebMethod User findByUsername(String username) throws EntityDoesNotExistException;
    @WebMethod Package createPackage( String sender,
			 String receiver,
			 String name,
			 String description,
			 String senderCity,
			 String destinationCity,
			 boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException;
    @WebMethod List<Package> findAllPackages();
    @WebMethod void updatePackage(Package packageEntity, int id);
    @WebMethod Package findByPackageId(int id);
    @WebMethod List<Package> findBySender(String sender);
    @WebMethod boolean deletePackage(int id) throws EntityDoesNotExistException;
    @WebMethod void updatePackageStatus(int packageId,Date date,String city);
}
