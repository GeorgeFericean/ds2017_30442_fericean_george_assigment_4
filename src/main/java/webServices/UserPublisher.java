package webServices;

import javax.xml.ws.Endpoint;  
//Endpoint publisher  
public class UserPublisher{  
  public static void main(String[] args) {  
     Endpoint.publish("http://localhost:7778/ws/user", new UserWebServiceImpl());  
     Endpoint.publish("http://localhost:7777/ws/admin", new AdminWebServiceImpl());  
      }  
}  