package webServices;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import business.PackageService;
import business.PackageServiceImpl;
import business.RouteService;
import business.RouteServiceImpl;
import business.UserService;
import business.UserServiceImpl;
import entities.Package;
import entities.User;
import exception.EntityDoesNotExistException;
import exception.FieldsMandatoryException;
@WebService(endpointInterface = "webServices.AdminWebService")  
public class AdminWebServiceImpl implements AdminWebService{
	UserService userService;
	PackageService packageService;
	RouteService routeService;
	public AdminWebServiceImpl() {
		this.userService=new UserServiceImpl();
		this.packageService=new PackageServiceImpl();
		this.routeService=new RouteServiceImpl();
	}
	@Override
	public User findByUserId(int id) {
		return userService.findByUserId(id);
	}
	@Override
	public User findByUsername(String username) throws EntityDoesNotExistException {
		return userService.findByUsername(username);
	}
	@Override
	public List<User> findAllUsers() {
		return userService.findAll();
	}
	@Override
	public Package createPackage(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity, boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException {
		return packageService.create(sender, receiver, name, description, senderCity, destinationCity, tracking);
	}
	@Override
	public List<Package> findAllPackages() {
		return packageService.findAll();
	}
	@Override
	public void updatePackage(Package packageEntity, int id) {
		packageService.update(packageEntity, id);
		
	}
	@Override
	public Package findByPackageId(int id) {
		return packageService.findByPackageId(id);
	}
	@Override
	public List<Package> findBySender(String sender) {
	return packageService.findBySender(sender);
	}
	@Override
	public boolean deletePackage(int id) throws EntityDoesNotExistException {
		return packageService.delete(id);
	}
	@Override
	public void updatePackageStatus(int packageId, Date date, String city) {
		 routeService.updateRoute(packageId, date, city);
		
	}  
}
