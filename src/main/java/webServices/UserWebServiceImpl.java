package webServices;

import java.util.List;

import javax.jws.WebService;

import business.PackageService;
import business.PackageServiceImpl;
import business.RouteService;
import business.RouteServiceImpl;
import business.UserService;
import business.UserServiceImpl;
import entities.Entry;
import entities.Package;
import entities.User;
import exception.EntityDoesNotExistException;
import exception.ExistingEntityException;
import exception.FieldsMandatoryException;

@WebService(endpointInterface = "webServices.UserWebService")  
public class UserWebServiceImpl implements UserWebService {
	UserService userService;
	PackageService packageService;
	RouteService routeService;
	public UserWebServiceImpl() {
		this.userService=new UserServiceImpl();
		this.packageService=new PackageServiceImpl();
		this.routeService=new RouteServiceImpl();
	}
	  @Override  
	    public boolean logIn(String username, String password) {  
	        return userService.logIn(username, password); 
	    }
	@Override
	public User createUser(String username, String password, String fullName)
			throws FieldsMandatoryException, ExistingEntityException {
		return userService.create(username, password, fullName);
	}
	@Override
	public void updateUser(User user, int id) {
		userService.update(user, id);
		
	}
 
	@Override
	public User findByUserId(int id) {
		return userService.findByUserId(id);
	}
	@Override
	public User findByUsername(String username) throws EntityDoesNotExistException {
		return userService.findByUsername(username);
	}
	@Override
	public List<User> findAllUsers() {
		return userService.findAll();
	}
	@Override
	public Package createPackage(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity, boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException {
		return packageService.create(sender, receiver, name, description, senderCity, destinationCity, tracking);
	}
	@Override
	public List<Package> findAllPackages() {
		return packageService.findAll();
	}
	@Override
	public void updatePackage(Package packageEntity, int id) {
		packageService.update(packageEntity, id);
		
	}
	@Override
	public Package findByPackageId(int id) {
		return packageService.findByPackageId(id);
	}
	@Override
	public List<Package> findBySender(String sender) {
	return packageService.findBySender(sender);
	}
	@Override
	public List<Package> findByReceiver(String receiver) {
	return packageService.findByReceiver(receiver);
	}
	@Override
	public List<Entry> CheckStatusForPackage(int packageId) {
		return routeService.getRouteForPackage(packageId);
	}  
}
