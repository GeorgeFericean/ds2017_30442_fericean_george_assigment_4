package webServices;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import entities.Entry;
import entities.Package;
import entities.User;
import exception.EntityDoesNotExistException;
import exception.ExistingEntityException;
import exception.FieldsMandatoryException;  
@WebService
@SOAPBinding(style = Style.DOCUMENT)  
public interface UserWebService {
    @WebMethod boolean logIn(String username, String password);  
    @WebMethod User createUser(String username,String password, String fullName) throws FieldsMandatoryException, ExistingEntityException;
    @WebMethod void updateUser(User user, int id);
    @WebMethod List<User> findAllUsers();
    @WebMethod User findByUserId(int id);
    @WebMethod User findByUsername(String username) throws EntityDoesNotExistException;
    @WebMethod Package createPackage( String sender,
			 String receiver,
			 String name,
			 String description,
			 String senderCity,
			 String destinationCity,
			 boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException;
    @WebMethod List<Package> findAllPackages();
    @WebMethod void updatePackage(Package packageEntity, int id);
    @WebMethod Package findByPackageId(int id);
    @WebMethod List<Package> findBySender(String sender);
    @WebMethod List<Entry> CheckStatusForPackage(int packageId);
    @WebMethod List<Package> findByReceiver(String receiver);
    
}
