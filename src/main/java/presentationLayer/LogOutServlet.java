package presentationLayer;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class LogOutServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7272969831867569963L;
	
	public LogOutServlet(){
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession(false);
		 session.removeAttribute("currentUsername");
		session.invalidate();
		try {
			response.sendRedirect("/Assignment4/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
