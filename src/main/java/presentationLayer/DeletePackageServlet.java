package presentationLayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import exception.EntityDoesNotExistException;
import webServices.AdminWebService;

public class DeletePackageServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2456680133320673L;
	private AdminWebService adminService;
	public DeletePackageServlet() {
		super();
		intializeService();
	}
	protected void intializeService() {
		URL url=null;
		try {
			url = new URL("http://localhost:7777/ws/admin?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		   
	        //1st argument service URI, refer to wsdl document above  
	    //2nd argument is service name, refer to wsdl document above  
	        QName qname = new QName("http://webServices/", "AdminWebServiceImplService");  
	        Service service = Service.create(url, qname);  
	        adminService = service.getPort(AdminWebService.class);  
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int id = 0;
		String url = request.getPathInfo();
		String idString = url.substring(1, url.length());
		id = Integer.parseInt(idString);

		try {
			adminService.deletePackage(id);
			response.sendRedirect("/Assignment4/admin");
			return;
		} catch (EntityDoesNotExistException e) {
			response.sendRedirect("/Assignment4/admin");
			return;

		}

	}


}
