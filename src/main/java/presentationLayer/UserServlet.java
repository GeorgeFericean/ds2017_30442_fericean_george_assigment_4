package presentationLayer;



import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;


import entities.Package;
import entities.User;
import exception.EntityDoesNotExistException;
import webServices.UserWebService;



public class UserServlet extends HttpServlet{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 4939650706400321934L;
	private UserWebService userService;
	public UserServlet() {
		super();
		intializeService();
	}
	protected void intializeService() {
		URL url=null;
		try {
			url = new URL("http://localhost:7778/ws/user?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		   
	        //1st argument service URI, refer to wsdl document above  
	    //2nd argument is service name, refer to wsdl document above  
	        QName qname = new QName("http://webServices/", "UserWebServiceImplService");  
	        Service service = Service.create(url, qname);  
	        userService = service.getPort(UserWebService.class);  
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession(false);
		if(session == null)
		{
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if(o == null)
		{
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (user == null || !user.getRole().equals("user")) {
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		
		List<Package> myPackages = userService.findByReceiver(username);
	//	myPackages.addAll(userService.findByReceiver(username));
		for(Package packa: myPackages) {
			System.out.println(packa.getId());
		}
		
		request.setAttribute("myPackages", myPackages);


		try {
			request.getRequestDispatcher("/jsp/user.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
