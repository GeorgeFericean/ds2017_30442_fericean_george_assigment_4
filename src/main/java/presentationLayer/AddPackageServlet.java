package presentationLayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import entities.User;
import entities.Package;
import exception.EntityDoesNotExistException;
import webServices.AdminWebService;

public class AddPackageServlet extends HttpServlet{

	private static final long serialVersionUID = 3082417795989407485L;
	private AdminWebService adminService;
	public AddPackageServlet() {
		super();
		intializeService();
	}
	protected void intializeService() {
		URL url=null;
		try {
			url = new URL("http://localhost:7777/ws/admin?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		   
	        //1st argument service URI, refer to wsdl document above  
	    //2nd argument is service name, refer to wsdl document above  
	        QName qname = new QName("http://webServices/", "AdminWebServiceImplService");  
	        Service service = Service.create(url, qname);  
	        adminService = service.getPort(AdminWebService.class);  
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");
			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if (o == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = adminService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			e1.printStackTrace();
		}

		if (user == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!user.getRole().equals("admin")) {
			session.removeAttribute("currentUsername");
			session.invalidate();
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<Package> packages = adminService.findAllPackages();
		request.setAttribute("packages", packages);
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/create-package.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		String sender = request.getParameter("sender");
		String receiver = request.getParameter("receiver");
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String senderCity = request.getParameter("senderCity");
		String destinationCity = request.getParameter("destinationCity");

		Package returnedPackage = null;
		try {
			returnedPackage = adminService.createPackage(sender, receiver, name, description, senderCity,
					destinationCity,false);
		} catch (Exception e) {
			 
			request.setAttribute("msg", "The provided data cannot be used to instantiate a consistent object. Please complete all the fields and check your input");
			try {
				request.getRequestDispatcher("/jsp/create-package.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		if (returnedPackage != null) {
			try {
				response.sendRedirect("/Assignment4/admin");
				return;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			request.setAttribute("msg", "Something went wrong.");
			try {
				request.getRequestDispatcher("/jsp/create-package.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

}
