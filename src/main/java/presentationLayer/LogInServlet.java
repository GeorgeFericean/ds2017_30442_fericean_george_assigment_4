package presentationLayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import entities.User;
import exception.EntityDoesNotExistException;
import webServices.UserWebService;


public class LogInServlet extends HttpServlet {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5076828648903542393L;
	private UserWebService userService;
		protected void intializeService() {
			URL url=null;
			try {
				url = new URL("http://localhost:7778/ws/user?wsdl");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			   
		        //1st argument service URI, refer to wsdl document above  
		    //2nd argument is service name, refer to wsdl document above  
		        QName qname = new QName("http://webServices/", "UserWebServiceImplService");  
		        Service service = Service.create(url, qname);  
		        userService = service.getPort(UserWebService.class);  
		}
	public LogInServlet() {
		super();
		intializeService();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/login.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username").toLowerCase();
		String password = request.getParameter("password");
		boolean logInStatus = userService.logIn(username, password);
		if (logInStatus == true) {
			User user = null;
			try {
				user = userService.findByUsername(username);
				HttpSession session = request.getSession();       
		        session.setAttribute("currentUsername",user.getUsername()); 
			} catch (EntityDoesNotExistException e) {
				request.setAttribute("msg", "The provided username does not exist");
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			}

			if (user.getRole().equals("admin")) {
				response.sendRedirect("/Assignment4/admin");
				return;
			} else if (user.getRole().equals("user")) {
				response.sendRedirect("/Assignment4/user");
				return;
			}

		} else {
			
			request.setAttribute("msg", "Invalid password");
			request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
			return;
		}

	}
}
