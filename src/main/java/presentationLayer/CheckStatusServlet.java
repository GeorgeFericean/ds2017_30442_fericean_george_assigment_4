package presentationLayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import entities.Entry;

import webServices.UserWebService;

public class CheckStatusServlet extends HttpServlet  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7023744957545115691L;
	private UserWebService userService;
	public CheckStatusServlet() {
		super();
		intializeService();
	}
	protected void intializeService() {
		URL url=null;
		try {
			url = new URL("http://localhost:7778/ws/user?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		   
	        //1st argument service URI, refer to wsdl document above  
	    //2nd argument is service name, refer to wsdl document above  
	        QName qname = new QName("http://webServices/", "UserWebServiceImplService");  
	        Service service = Service.create(url, qname);  
	        userService = service.getPort(UserWebService.class);  
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		int id = 0;
		String url = request.getPathInfo();
		String idString = url.substring(1, url.length());
		id = Integer.parseInt(idString);
		List<Entry> entries=userService.CheckStatusForPackage(id);
		request.setAttribute("entries", entries);
		if(entries==null) {
			request.setAttribute("msg", "this package is not registered for tracking");
		}
		else {
			request.setAttribute("entries", entries);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/check-status.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
