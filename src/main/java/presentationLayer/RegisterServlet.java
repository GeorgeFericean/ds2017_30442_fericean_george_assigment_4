package presentationLayer;



import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import entities.User;
import exception.ExistingEntityException;
import exception.FieldsMandatoryException;
import webServices.UserWebService;



public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7908564807968267869L;
	private UserWebService userService;
	public RegisterServlet() {
		super();
		intializeService();
	}
	protected void intializeService() {
		URL url=null;
		try {
			url = new URL("http://localhost:7778/ws/user?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		   
	        //1st argument service URI, refer to wsdl document above  
	    //2nd argument is service name, refer to wsdl document above  
	        QName qname = new QName("http://webServices/", "UserWebServiceImplService");  
	        Service service = Service.create(url, qname);  
	        userService = service.getPort(UserWebService.class);  
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response){
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/register.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response){
		
		String fullname = request.getParameter("fullname");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
 
		
		if(!password.equals(confirmPassword))
		{
			request.setAttribute("msg", "Passwords do not match.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		 
		User user = null;
		try {
			user = userService.createUser(username, password, fullname);
		} catch (ExistingEntityException e) {
			request.setAttribute("msg", "Username already exists.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 
		} catch (FieldsMandatoryException e) {
			request.setAttribute("msg", "All fields are mandatory.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(user == null)
		{
			request.setAttribute("msg", "Something went wrong. Please try again.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		try {
			response.sendRedirect("/Assignment4/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}
}
