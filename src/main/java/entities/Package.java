package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "packages")
public class Package  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2134456446822676701L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String sender;
	private String receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destinationCity;
	private boolean tracking;
	public Package() {}
	public Package(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity, boolean tracking) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.name = name;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking=tracking;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}
	
}
