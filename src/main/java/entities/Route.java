package entities;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "routes")
public class Route implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3673373858840448873L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int packageId;
	private int entryId;
	public Route() {
	}
	public Route(int entryId,int packageId) {
		this.setPackageId(packageId);
		this.setEntryId(entryId);
	}
	public int getEntryId() {
		return entryId;
	}
	public void setEntryId(int entryId) {
		this.entryId = entryId;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
}
