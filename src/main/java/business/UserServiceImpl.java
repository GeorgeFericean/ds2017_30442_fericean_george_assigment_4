package business;
import java.util.List;

import entities.User;
import exception.EntityDoesNotExistException;
import exception.ExistingEntityException;
import exception.FieldsMandatoryException;
import dao.UserDao;


public class UserServiceImpl implements UserService{
	private UserDao userDao;
	

	public UserServiceImpl(){
		userDao=new UserDao();
	}
	public User create(String username,String password, String fullName) throws FieldsMandatoryException, ExistingEntityException {
		if(username==null || username.equals("")
				|| password==null || password.equals("")
				|| fullName==null || fullName.equals(""))
		{
			throw new FieldsMandatoryException("All fields are mandatory.");
		}
		else {
			List<User> users = this.findAll();
			if (users != null) {
				for (User user : users) {
					if (user.getUsername().toLowerCase().equals(username.toLowerCase())) {
						throw new ExistingEntityException("The username already exists");
					}
				}
			}
		User userNew=new User(username,password,fullName,"user"); 
		userDao.openCurrentSessionwithTransaction();
		 userDao.save(userNew);
		 userDao.closeCurrentSessionwithTransaction();
		return userNew;
		}
	}
	public void update(User user, int id) {
		userDao.openCurrentSessionwithTransaction();
		User changedUser=(User) userDao.findById(id);
		changedUser.setUsername(user.getUsername());
		changedUser.setPassword(user.getPassword());
		changedUser.setFullName(user.getFullName());
		userDao.update(changedUser);
		 userDao.closeCurrentSessionwithTransaction();
		
	}
	@Override
	public List<User> findAll() {
		userDao.openCurrentSessionwithTransaction();
		List<User> users=userDao.findAll();
		 userDao.closeCurrentSessionwithTransaction();
		return users;
	}
	@Override
	public User findByUserId(int id) {
		userDao.openCurrentSessionwithTransaction();
		User user=userDao.findById(id);
		 userDao.closeCurrentSessionwithTransaction();
		return user;
	}
	@Override
	public User findByUsername(String username) throws EntityDoesNotExistException {
		List<User> users = this.findAll();
		if(users!=null)
		{
			for(User user: users)
			{
				if(user.getUsername().equals(username))
					return user;
			}
		}
		
		throw new EntityDoesNotExistException("The username does not exist.");
	}
	@Override
	public boolean logIn(String username, String password) {

		List<User> users = this.findAll();
		for (User user : users) {
			if (user.getUsername().toLowerCase().equals(username.toLowerCase())) {
				{
					if (user.getPassword().equals(password)) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
		return false;
	}

}
