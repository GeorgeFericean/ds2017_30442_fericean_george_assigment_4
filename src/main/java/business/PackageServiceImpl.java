package business;

import java.util.ArrayList;
import java.util.List;

import dao.PackageDao;
import dao.UserDao;
import entities.User;
import exception.EntityDoesNotExistException;
import exception.FieldsMandatoryException;
import entities.Package;

public class PackageServiceImpl implements PackageService {
	private UserDao userDao;
	private PackageDao packageDao;
	
	public PackageServiceImpl() {
		this.userDao=new UserDao();
		this.packageDao=new PackageDao();
	}
	@Override
	public Package create( 
	 String sender,
	 String receiver,
	 String name,
	 String description,
	 String senderCity,
	 String destinationCity,
	 boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException {
		if(sender==null || sender.equals("")
		|| receiver==null || receiver.equals("")
		|| name==null || name.equals("")
		|| description==null || description.equals("")
		|| senderCity==null || senderCity.equals("")
		|| destinationCity==null || destinationCity.equals(""))
		{
			throw new FieldsMandatoryException("All fields are mandatory.");
		}
		userDao.openCurrentSessionwithTransaction();
		List<User> users = userDao.findAll();
		 userDao.closeCurrentSessionwithTransaction();
		boolean senderFound=false;
		boolean receiverFound=false;
		if(users!=null)
		{
			for(User user: users)
			{
				if(user.getUsername().equals(sender))
					senderFound=true;
				if(user.getUsername().equals(receiver))
					receiverFound=true;
			}
		}
		if(!senderFound && !receiverFound)
			throw new EntityDoesNotExistException("The sender and receiver you provided does not exist.");
		if(!senderFound)
			throw new EntityDoesNotExistException("The sender you provided does not exist.");
		if(!receiverFound)
			throw new EntityDoesNotExistException("The sender you provided does not exist.");
		packageDao.openCurrentSessionwithTransaction();
		Package packageEntity=new Package(sender,receiver,name,description,senderCity,destinationCity,false);
		
		Package savedPackage= packageDao.save(packageEntity);
		 packageDao.closeCurrentSessionwithTransaction();
		return savedPackage;
	}

	@Override
	public List<Package> findAll() {
		packageDao.openCurrentSessionwithTransaction();
		List<Package> packages=packageDao.findAll();
		 packageDao.closeCurrentSessionwithTransaction();
		return packages;
	}

	@Override
	public void update(Package packageEntity, int id) {
		packageDao.openCurrentSessionwithTransaction();
		Package changedPackage=(Package) packageDao.findById(id);
		changedPackage.setSender(packageEntity.getSender());
		changedPackage.setReceiver(packageEntity.getReceiver());
		changedPackage.setName(packageEntity.getName());
		changedPackage.setDescription(packageEntity.getDescription());
		changedPackage.setSenderCity(packageEntity.getSenderCity());
		changedPackage.setDestinationCity(packageEntity.getDestinationCity());
		changedPackage.setTracking(packageEntity.isTracking());
		packageDao.update(packageEntity);
		packageDao.closeCurrentSessionwithTransaction();


		
	}

	@Override
	public Package findByPackageId(int id) {
		packageDao.openCurrentSessionwithTransaction();
		Package packageEntry=packageDao.findById(id);
		 packageDao.closeCurrentSessionwithTransaction();
		return packageEntry;
	}

	@Override
	public List<Package> findBySender(String sender) {
		packageDao.openCurrentSessionwithTransaction();
		List<Package> packages=packageDao.findAll();
		 packageDao.closeCurrentSessionwithTransaction();
		 List<Package> packagesFound=new ArrayList<Package>();
		for(Package packageEntry:packages) {
			if(packageEntry.getSender().toLowerCase().compareTo(sender.toLowerCase())==0)
			{
				packagesFound.add(packageEntry);
			}
		}
		return packagesFound;
	}
	@Override
	public List<Package> findByReceiver(String receiver) {
		packageDao.openCurrentSessionwithTransaction();
		List<Package> packages=packageDao.findAll();
		 packageDao.closeCurrentSessionwithTransaction();
		 List<Package> packagesFound=new ArrayList<Package>();
		for(Package packageEntry:packages) {
			if(packageEntry.getReceiver().toLowerCase().compareTo(receiver.toLowerCase())==0)
			{
				packagesFound.add(packageEntry);
			}
		}
		return packagesFound;
	}
	@Override
	public boolean delete(int id) {
		packageDao.openCurrentSessionwithTransaction();
		Package pack=packageDao.findById(id);
		boolean deletePackage=packageDao.delete(pack);
		 packageDao.closeCurrentSessionwithTransaction();
		return deletePackage;
	}

}
