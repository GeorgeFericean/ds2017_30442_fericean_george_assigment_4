package business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.EntryDao;
import dao.RouteDao;
import entities.Entry;
import entities.Route;

public class RouteServiceImpl implements RouteService {
	
		private RouteDao routeDao;
		private EntryDao entryDao;
	

	public RouteServiceImpl(){
		routeDao=new RouteDao();
		entryDao=new EntryDao();
	}
	@Override
	public void updateRoute(int packageId, Date date,String city ) {
		entryDao.openCurrentSessionwithTransaction();
		Entry entry=new Entry(city,date);
		entry=entryDao.save(entry);
		entryDao.closeCurrentSessionwithTransaction();
		Route route=new Route(entry.getId(),packageId);
		routeDao.openCurrentSessionwithTransaction();
		routeDao.save(route);
		routeDao.closeCurrentSessionwithTransaction();
	}

	@Override
	public List<Entry> getRouteForPackage(int packageId) {
		routeDao.openCurrentSessionwithTransaction();
		List<Route> routes=routeDao.findByPackageId(packageId);
		routeDao.closeCurrentSessionwithTransaction();
		List<Entry> entries=new ArrayList<Entry>();
		entryDao.openCurrentSessionwithTransaction();
		for(Route route:routes) {
			Entry entry=entryDao.findById(route.getEntryId());
			entries.add(entry);
		}
		entryDao.closeCurrentSessionwithTransaction();
		return entries;
	}

}
