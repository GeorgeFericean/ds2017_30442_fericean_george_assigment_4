package business;

import java.util.List;

import exception.EntityDoesNotExistException;
import exception.FieldsMandatoryException;
import entities.Package;

public interface PackageService {
		Package create( String sender,
				 String receiver,
				 String name,
				 String description,
				 String senderCity,
				 String destinationCity,
				 boolean tracking) throws FieldsMandatoryException, EntityDoesNotExistException;
		List<Package> findAll();
		void update(Package packageEntity, int id);
		Package findByPackageId(int id);
		List<Package> findBySender(String sender);
		List<Package> findByReceiver(String receiver);
		boolean delete(int id);
}
