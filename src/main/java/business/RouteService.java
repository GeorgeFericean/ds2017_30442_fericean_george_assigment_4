package business;

import java.util.Date;
import java.util.List;

import entities.Entry;


public interface RouteService {
	void updateRoute(int packageId,Date date,String city);
	List<Entry> getRouteForPackage(int packageId);
}
