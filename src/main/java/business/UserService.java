package business;

import java.util.List;

import entities.User;
import exception.EntityDoesNotExistException;
import exception.ExistingEntityException;
import exception.FieldsMandatoryException;



public interface UserService {
	User create(String username,String password, String fullName ) throws FieldsMandatoryException, ExistingEntityException;
	List<User> findAll();
	void update(User user, int id);
	User findByUserId(int id);
	User findByUsername(String username) throws EntityDoesNotExistException;
	boolean logIn(String username, String password);
}
