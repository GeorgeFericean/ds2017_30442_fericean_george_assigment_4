package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import entities.Package;

public class PackageDao implements PackageDaoInterface<Package, String> {
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public PackageDao() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public Package save(Package entity) {
		getCurrentSession().save(entity);
		return entity;
	}

	@Override
	public boolean update(Package entity) {
		try {
			getCurrentSession().update(entity);
			return true;
			}
			catch (Exception e) {
				return false;
			}
	}
	@Override
	public boolean delete(Package entity) {
		try {
			getCurrentSession().delete(entity);
			return true;
			}
			catch  (Exception e){
				return false;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Package> findAll() {
		List<Package> pacakges = (List<Package>) getCurrentSession().createQuery(
				"from "+Package.class.getName()).list();
		return pacakges;
	}

	@Override
	public Package findById(int id) {
			Package packageEntity = (Package) getCurrentSession().get(Package.class, id);
			return packageEntity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Package> findByName(String name) {
		List<Package> pacakges = (List<Package>) getCurrentSession().createQuery(
				"from "+Package.class.getName()+"where name = "+name).list();
		return pacakges;
	}
}
