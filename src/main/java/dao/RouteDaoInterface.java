package dao;

import java.io.Serializable;
import java.util.List;

public interface RouteDaoInterface<T, Id extends Serializable> {
	public T save(T entity);
	public boolean update(T entity);
	public T findById(String id);
	public List<T> findByPackageId(int packageId);
	public boolean delete(T entity);
	public List<T> findAll();
}