package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import entities.Route;

public class RouteDao implements RouteDaoInterface<Route, String> {
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public RouteDao() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public Route save(Route entity) {
		getCurrentSession().save(entity);
		return entity;
	}

	@Override
	public boolean update(Route entity) {
		try {
			getCurrentSession().update(entity);
			return true;
			}
			catch (Exception e) {
				return false;
			}
	}

	@Override
	public Route findById(String id) {
		Route route = (Route) getCurrentSession().get(Route.class, id);
		return route;
	}

	@Override
	public boolean delete(Route entity) {
		try {
			getCurrentSession().delete(entity);
			return true;
			}
			catch  (Exception e){
				return false;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Route> findByPackageId(int packageId) {
		List<Route> routes = (List<Route>) getCurrentSession().createQuery(
				"from "+Route.class.getName()+" where packageId = "+packageId).list();
		return routes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Route> findAll() {
		List<Route> routes = (List<Route>) getCurrentSession().createQuery(
				"from "+Route.class.getName()).list();
		return routes;
	}

}
