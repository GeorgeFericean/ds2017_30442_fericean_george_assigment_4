package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import entities.Entry;

public class EntryDao implements EntryDaoInterface<Entry, String> {
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public EntryDao() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	@Override
	public Entry save(Entry entity) {
		getCurrentSession().save(entity);
		return entity;
	}

	@Override
	public boolean update(Entry entity) {
		try {
			getCurrentSession().update(entity);
			return true;
			}
			catch (Exception e) {
				return false;
			}
	}

	@Override
	public Entry findById(int id) {
		Entry entry = (Entry) getCurrentSession().get(Entry.class, id);
		return entry;
	}

	@Override
	public boolean delete(Entry entity) {
		try {
			getCurrentSession().delete(entity);
			return true;
			}
			catch  (Exception e){
				return false;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Entry> findAll() {
		List<Entry> entries = (List<Entry>) getCurrentSession().createQuery(
				"from "+Entry.class.getName()).list();
		return entries;
	}

}
