package dao;

import java.io.Serializable;
import java.util.List;

public interface EntryDaoInterface<T, Id extends Serializable> {
	public T save(T entity);
	public boolean update(T entity);
	public T findById(int id);
	public boolean delete(T entity);
	public List<T> findAll();
}
