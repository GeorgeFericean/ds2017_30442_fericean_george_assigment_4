package dao;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import entities.User;




public class UserDao implements UserDaoInterface<User, String> {
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public UserDao() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public User save(User entity) {
//		Integer id=null;
//		try {id=(Integer) 
			getCurrentSession().save(entity);
			
//		}
//		catch(Exception e) {
//		//	this.update(entity);
//		}
		return entity;
	}

	public boolean update(User entity) {
		try {
		getCurrentSession().update(entity);
		return true;
		}
		catch (Exception e) {
			return false;
		}
	}

	public User findById(int id) {
		User user = (User) getCurrentSession().get(User.class, id);
		return user;
	}

	public boolean delete(User entity) {
		try {
			getCurrentSession().delete(entity);
			return true;
			}
			catch  (Exception e){
				return false;
			}
			
	}

	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		List<User> users = (List<User>) getCurrentSession().createQuery(
				"from "+User.class.getName()).list();
		return users;
	}

}
