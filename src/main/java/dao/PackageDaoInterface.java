package dao;

import java.io.Serializable;
import java.util.List;

public interface PackageDaoInterface<T, Id extends Serializable> {
		public T save(T entity);
		public boolean update(T entity);
		public T findById(int id);
		public boolean delete(T entity);
		public List<T> findAll();
		public List<T> findByName(String name);
}
