package exception;

public class FieldsMandatoryException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1897440508713238503L;
	
	public FieldsMandatoryException(String msg)
	{
		super(msg);
	}
}
